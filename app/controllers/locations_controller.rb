class LocationsController < ApplicationController
  before_action :set_location, only: [:show, :update, :destroy]
  # GET /locations
  def index
    @locations = Location.all

    render json: @locations
  end

  # GET /locations/1
  def show
    @location = Location.find(params[:id])

    require 'http'

    longitude = @location.longitude
    latitude = @location.latitude
    time = @location.time
    key = ENV['DS_SECRET_KEY']
    api = "https://api.darksky.net/forecast/#{key}/#{latitude},#{longitude},#{time}"
    forecast = HTTP.get(api)
    @weather = JSON.parse forecast
    render json: @weather
  end

  # POST /locations
  def create
    @location = Location.new(location_params)

    if @location.save
      render json: @location, status: :created
    else
      render json: @location.errors, status: :unprocessable_entity
    end

  end

  # PATCH/PUT /locations/1
  def update
    if @location.update(location_params)
      render json: @location
    else
      render json: @location.errors, status: :unprocessable_entity
    end
  end

  # DELETE /locations/1
  def destroy
    @location.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def location_params
      params.require(:location).permit(:address, :time)
    end
end
